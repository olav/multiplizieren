let zahl2 = 0
let zahl1 = 0
let ergebnis1 = 0
let ergebnis10 = 0
input.onGesture(Gesture.TiltLeft, () => {
    basic.setLedColor(0)
    zahl1 = Math.random(10)
    zahl2 = Math.random(10)
    ergebnis10 = 0
    ergebnis1 = 0
    basic.showString("" + zahl1 + "x" + zahl2)
    led.plot(0, 0)
    led.plot(3, 0)
})
input.onGesture(Gesture.TiltRight, () => {
    if (ergebnis10 * 10 + ergebnis1 == zahl1 * zahl2) {
        basic.showLeds(`
            . # . # .
            . . . . .
            # . . . #
            . # # # .
            . . . . .
            `)
        music.playTone(Note.C, music.beat(BeatFraction.Quarter))
        music.playTone(Note.G, music.beat(BeatFraction.Quarter))
    } else {
        basic.showLeds(`
            . # . # .
            . . . . .
            . . . . .
            . # # # .
            # . . . #
            `)
        music.playTone(Note.G, music.beat(BeatFraction.Quarter))
        music.playTone(Note.C, music.beat(BeatFraction.Quarter))
    }
})
input.onButtonPressed(Button.A, () => {
    ergebnis10 += 1
    if (ergebnis10 > 9) {
        ergebnis10 = 0
    }
    basic.clearScreen()
    led.plot(0, ergebnis10)
    led.plot(1, ergebnis10 - 5)
    led.plot(3, ergebnis1)
    led.plot(4, ergebnis1 - 5)
})
input.onButtonPressed(Button.B, () => {
    ergebnis1 += 1
    if (ergebnis1 > 9) {
        ergebnis1 = 0
    }
    basic.clearScreen()
    led.plot(3, ergebnis1)
    led.plot(4, ergebnis1 - 5)
    led.plot(0, ergebnis10)
    led.plot(1, ergebnis10 - 5)
})
